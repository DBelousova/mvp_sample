﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess;

namespace Model
{
    #region ProjectsEventArgs

    public class StudentEventArgs : EventArgs
    {
        public Student Students { get; set; }
        public StudentEventArgs(Student project)
        {
            Students = project;
        }
    }

    #endregion ProjectsEventArgs

    public interface IStudentModel
    {
        void UpdateStudents(Student project);
        IEnumerable<Student> GetProjects();
        Student GetProject(int Id);
        event EventHandler<StudentEventArgs> ProjectUpdated;
    }

    public class ProjectsModel : IStudentModel
    {
        private IEnumerable<Student> _students = null;

        public event EventHandler<StudentEventArgs>
            ProjectUpdated = delegate { };

        public ProjectsModel()
        {
            _students = new DataServiceStub().GetStudents();
        }

        public void UpdateStudents(Student project)
        {
            ProjectUpdated(this,
                new StudentEventArgs(project));
        }

        public IEnumerable<Student> GetProjects()
        {
            return _students;
        }


        public Student GetProject(int Id)
        {
            return _students.Where(p => p.ID == Id)
                .First() as Student;
        }
    }
}
