﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using DataAccess;
using Model;


namespace View
{


    public interface IStudentsView
    {
        int NONE_SELECTED { get; }
        int SelectedStudentId { get; }
        void UpdateStudent(Student student);
        void LoadStudents(IEnumerable<Student> student);
        void UpdateDetails(Student student);
        void EnableControls(bool isEnabled);
        void SetEstimatedColor(Color? color);
        event EventHandler<StudentEventArgs> StudentUpdated;
        event EventHandler<StudentEventArgs> DetailsUpdated;
        event EventHandler SelectionChanged;
    }




    public partial class ProjectsView : Window, IStudentsView
    {
        #region Initialization

        public int NONE_SELECTED
        {
            get { return -1; }
        }

        public event EventHandler<StudentEventArgs>
            StudentUpdated = delegate { };

        public int SelectedStudentId { get; private set; }

        public event EventHandler SelectionChanged
            = delegate { };

        public event EventHandler<StudentEventArgs>
            DetailsUpdated = delegate { };

        public ProjectsView()
        {
            InitializeComponent();
            SelectedStudentId = NONE_SELECTED;
        }

        #endregion Initialization

        #region Event handlers

        private void updateButton_Click(object sender,
            RoutedEventArgs e)
        {
            Student _student = new Student();
            _student.Name = nameTB.Text;
            _student.Institute = InstituteTB.Text;
            _student.Group = GroupTB.Text;
            _student.ID =
                int.Parse(
                    ListOfStudents.SelectedValue.
                        ToString());
            StudentUpdated(this,
                new StudentEventArgs(_student));
        }

        private void projectsComboBox_SelectionChanged(
            object sender, SelectionChangedEventArgs e)
        {
            SelectedStudentId
                = (ListOfStudents.SelectedValue == null)
                    ? NONE_SELECTED
                    : int.Parse(
                        ListOfStudents.SelectedValue.
                            ToString());
            SelectionChanged(this,
                new EventArgs());
        }

        #endregion Event handlers

        #region Public methods

        public void UpdateStudent(Student student)
        {
            IEnumerable<Student> projects =
                ListOfStudents.ItemsSource as
                    IEnumerable<Student>;
            Student studentToUpdate =
                projects.Where(p => p.ID == student.ID)
                    .First() as Student;
            studentToUpdate.Name = student.Name;
            studentToUpdate.Institute = student.Institute;
            studentToUpdate.Group = student.Group;
            if (student.ID == SelectedStudentId)
                UpdateDetails(student);
        }

        public void LoadStudents(IEnumerable<Student> students)
        {
            ListOfStudents.ItemsSource = students;
            ListOfStudents.DisplayMemberPath = "Name";
            ListOfStudents.SelectedValuePath = "ID";
        }

        public void EnableControls(bool isEnabled)
        {
            InstituteTB.IsEnabled = isEnabled;
            GroupTB.IsEnabled = isEnabled;
            updateButton.IsEnabled = isEnabled;
        }

        public void SetEstimatedColor(Color? color)
        {
            //estimatedTextBox.Foreground
            //    = (color == null)
            //        ? actualTextBox.Foreground
            //        : new SolidColorBrush((Color)color);
        }

        public void UpdateDetails(Student student)
        {
            nameTB.Text = student.Name;
            InstituteTB.Text
                = student.Institute.ToString();
            GroupTB.Text
                = student.Group.ToString();
            DetailsUpdated(this,
                new StudentEventArgs(student));
        }

        #endregion Public methods

        #region Helpers

        private double GetDouble(string text)
        {
            return string.IsNullOrEmpty(text)
                ? 0
                : double.Parse(text);
        }

        #endregion Helpers

    }

}
