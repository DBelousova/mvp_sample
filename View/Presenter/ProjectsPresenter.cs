﻿using System;
using System.Windows.Media;
using DataAccess;
using Model;

namespace View.Presenter
{
    public class ProjectsPresenter
    {
        #region Initialization

        private IStudentsView view = null;
        private IStudentModel model = null;

        public ProjectsPresenter(IStudentsView projectsView,
            IStudentModel projectsModel)
        {
            view = projectsView;
            view.StudentUpdated += view_ProjectUpdated;
            view.SelectionChanged
                += view_SelectionChanged;
            view.DetailsUpdated += view_DetailsUpdated;
            model = projectsModel;
            model.ProjectUpdated += model_ProjectUpdated;
            view.LoadStudents(
                model.GetProjects());
        }

        #endregion Initialization

        #region Event handlers

        private void view_DetailsUpdated(object sender,
            StudentEventArgs e)
        {
            SetEstimatedColor(e.Students);
        }

        private void view_SelectionChanged(object sender,
            EventArgs e)
        {
            int selectedId = view.SelectedStudentId;
            if (selectedId > view.NONE_SELECTED)
            {
                Student project =
                    model.GetProject(selectedId);
                view.EnableControls(true);
                view.UpdateDetails(project);
                SetEstimatedColor(project);
            }
            else
            {
                view.EnableControls(false);
            }
        }

        private void model_ProjectUpdated(object sender,
            StudentEventArgs e)
        {
            view.UpdateStudent(e.Students);
        }

        private void view_ProjectUpdated(object sender,
            StudentEventArgs e)
        {
            model.UpdateStudents(e.Students);
            SetEstimatedColor(e.Students);
        }

        #endregion Event handlers

        #region Helpers

        private void SetEstimatedColor(Student project)
        {
            //if (project.ID == view.SelectedStudentId)
            //{
            //    if (project.Actual <= 0)
            //    {
            //        view.SetEstimatedColor(null);
            //    }
            //    else if (project.Actual
            //             > project.Estimate)
            //    {
            //        view.SetEstimatedColor(Colors.Red);
            //    }
            //    else
            //    {
            //        view.SetEstimatedColor(Colors.Green);
            //    }
            //}
        }

        #endregion Helpers
    }
}
