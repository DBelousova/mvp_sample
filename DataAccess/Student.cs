﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public interface IStudent
    {
        int ID { get; set; }
        string Name { get; set; }
        string Institute { get; set; }
        string Group { get; set; }
        void Update(IStudent student);
    }

    public class Student : IStudent
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Institute { get; set; }
        public string Group { get; set; }
        public void Update(IStudent student)
        {
            this.ID = student.ID;
            this.Name = student.Name;
            this.Institute = student.Institute;
            this.Group = student.Group;
        }
    }
}
