﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess
{
    public interface IDataService
    {
        IList<Student> GetStudents();
    }

    public class DataServiceStub : IDataService
    {
        public IList<Student> GetStudents()
        {
            List<Student> _students = new List<Student>();

            for (int i = 0; i < 5; i++)
            {
                _students.Add(new Student() { ID = i, Name = "Student " + i.ToString(), Group = "KI13-14", Institute = "IKIT" });
            }

            return _students;
        }
    }
}
